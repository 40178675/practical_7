﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace airport
{
    class Program
    {
        static void Main(string[] args)
        {
            List<flights> fl = new List<flights> ();
            passenger p = new passenger();
            p.Airline = "RyanAir";
            p.Destination = "Sweden";
            p.DepartureTime = "18/09/2087";
            p.PassengerCount = 250;
            freight f = new freight();
            f.Airline = "DHL";
            f.DepartureTime = "20:00";
            f.Destination = "Germany";
            f.Weight=1500.99;
            fl.Add(p);
            fl.Add(f);
            Console.ReadLine();
            foreach (flights x in fl)
            {
                x.print();
            }
            Console.ReadLine();
        }
    }
}
