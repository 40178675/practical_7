﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace airport
{
    class freight : flights,Interface1
    {
        private string theAirline;
        private string theDestination;
        private string theDepartureTime;
        private double theWeight;
        public string Airline
        {
            get { return theAirline; }
            set { theAirline = value; }
        }
        public string Destination
        {
            get { return theDestination; }
            set { theDestination = value; }
        }
        public string DepartureTime
        {
            get { return theDepartureTime; }
            set { theDepartureTime = value; }
        }
        public double Weight
        {
            get { return theWeight; }
            set { theWeight = value; }
        }
        public override void print()
        {
            Console.WriteLine("Freight Flight:" + Airline + " , " + Weight);
        }

    }
}
