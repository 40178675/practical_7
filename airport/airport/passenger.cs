﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace airport
{
    class passenger: flights,Interface1
    {        
        private string theAirline;
        private string theDestination;
        private string theDepartureTime;
        private int thePassengerCount;
        public string Airline
        {
            get { return theAirline; }
            set { theAirline = value; }
        }
        public string Destination
        {
            get { return theDestination; }
            set { theDestination = value; }
        }
        public string DepartureTime
        {
            get { return theDepartureTime; }
            set { theDepartureTime = value; }
        }
        public int PassengerCount
        {
            get { return thePassengerCount; }
            set { thePassengerCount = value; }
        }

        public override void print()
        {
            Console.WriteLine("Passenger Flight:" + Airline+" , " + PassengerCount);
        }

    }
}
